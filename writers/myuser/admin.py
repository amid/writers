# coding: utf8
from .forms import AdminUserChangeForm, AdminUserAddForm
from .models import Position, UserProfile
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _


class UserAdmin(BaseUserAdmin):
    form = AdminUserChangeForm
    add_form = AdminUserAddForm
    list_display = ('username', 'first_name', 'last_name', 'position', 'email',
                    'skype', 'is_active', 'is_staff', 'is_superuser',)
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': (
            'first_name',
            'last_name',
            'email',
            'type_performer',
            'position',
            'skype',
            'number_cart',
        )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'email', 'password1', 'password2')}
         ),
    )


class PositionAdmin(admin.ModelAdmin):
    list_display = ('name_position',)

admin.site.register(UserProfile, UserAdmin)
admin.site.register(Position, PositionAdmin)
