# coding: utf8
from models import UserProfile
from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm


# Допиливаем форму добавления пользователя.
# В Meta.model указываем нашу модель.
class AdminUserAddForm(UserCreationForm):

    class Meta:
        model = UserProfile
        fields = ("username", "email", "password1", "password2")

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            UserProfile._default_manager.get(username=username)
        except UserProfile.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


# Допиливаем форму редактирования пользователя.
# В Meta.model указываем нашу модель.
class AdminUserChangeForm(UserChangeForm):

    class Meta:
        model = UserProfile
        fields = "__all__"
