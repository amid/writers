# coding: utf8
from django.contrib.auth.models import AbstractUser
from django.db import models

AbstractUser._meta.get_field('email')._unique = True
AbstractUser._meta.get_field('email').blank = False

sort_performer = (('freelancer', u'Фрилансер'), ('office', u'Офис'))


class Position(models.Model):
    '''Наименование должностей'''
    name_position = models.CharField(blank=True, null=True, max_length=150,
                                     verbose_name=u'Название должности')

    class Meta:
        verbose_name = 'Наименование должностей'
        verbose_name_plural = 'Наименование должностей'
        ordering = ['name_position']
        permissions = (('perms_position', 'права на должности'),)

    def __unicode__(self):
        return u'%s' % self.name_position


class UserProfile(AbstractUser):
    skype = models.CharField(blank=True, max_length=255,
                             verbose_name=u'Имя в skype')
    number_cart = models.CharField(blank=True, max_length=255,
                                   verbose_name=u'Номер зарплатной карты')
    type_performer = models.CharField(choices=sort_performer, max_length=100,
                                      verbose_name='Тип сотрудника')
    position = models.ForeignKey(Position, blank=True, null=True,
                                 verbose_name=u'Должность сотрудника')

    class Meta:
            verbose_name = 'Сотрудники'
            verbose_name_plural = 'Сотрудники'
            ordering = ['username']
            permissions = (('perms_userprofile',
                            u'права на редактирование сотрудников'),)

    def __unicode__(self):
        return u'%s %s' % (self.first_name,
                           self.last_name)

    def get_full_name(self):
        return u'%s %s' % (self.first_name, self.last_name,)

    def get_short_name(self):
        return u'%s' % self.username
