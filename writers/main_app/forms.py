# coding: utf8
from django import forms
from datetime import *
from django.forms import ModelForm
from .models import Wages
import autocomplete_light


class FormsAddWages(ModelForm):
    '''Форма модели для замены техники'''
    def __init__(self, *args, **kwargs):
        super(FormsAddWages, self).__init__(*args, **kwargs)
        self.fields['cost'].widget=forms.TextInput(attrs={'oninput':'calculate()'})
        self.fields['number_position'].widget=forms.TextInput(attrs={'oninput':'calculate()'})
        self.fields['date'].initial = datetime.now()
        self.fields['wage'].widget.attrs['readonly'] = True
        self.fields['cost'].required = True
        self.fields['number_position'].required = True

    class Meta:
        #~ widgets = {
            #~ 'previous_department': autocomplete_light.ChoiceWidget('DepartmentAutocomplete', attrs={'size':'40'}),
            #~ 'next_department': autocomplete_light.ChoiceWidget('DepartmentAutocomplete', attrs={'size':'40'}),
            #~ 'name_device': autocomplete_light.ChoiceWidget('DeviceAutocomplete', attrs={'size':'40'}),
        #~ }
        model = Wages
        fields = "__all__"

