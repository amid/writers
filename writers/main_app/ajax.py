# coding: utf8
from django.http import HttpResponse


def xhr_test(request):
    if request.is_ajax():
        if request.method == 'GET':
            message = "This is an XHR GET request"
        elif request.method == 'POST':
            message = "This is an XHR POST request"
            print request.POST
    else:
        message = "Hello"
    return HttpResponse(message)
