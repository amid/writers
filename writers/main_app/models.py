# coding: utf8
from django.db import models


class JobTitle(models.Model):
    '''Проекты'''
    name = models.CharField(unique=True, max_length=150,
                            help_text=u'проект nashbutik.ru, копирайтинг',
                            verbose_name=u'Проекты')
    date_execution = models.DateTimeField(verbose_name=u'Дата начала работы')
    wage_rate = models.PositiveIntegerField(blank=True, null=True,
                                       verbose_name=u'Ставка за единицу',
                                       help_text=u'цена оплаты за \
                                       одну статью или 1000 символов',
                                       )
    performers = models.ManyToManyField('myuser.UserProfile',
                                        help_text=u'Фамилии исполнителей',
                                        verbose_name=u'Исполнители',
                                        blank=True)
    note = models.CharField(null=True, blank=True, max_length=255,
                            verbose_name='Примечание')
    object_delete = models.BooleanField(verbose_name=u'Отметка об удалении',
                                        default=False)

    class Meta:
        verbose_name = u'Названия проектов'
        verbose_name_plural = u' Названия проектов'
        ordering = ['name']
        db_table = 'jobtitle'
        permissions = (('perms_job_title', u'права на название проектов'),)

    def __unicode__(self):
        return u'%s' % self.name


class Wages(models.Model):
    '''Зарплата'''
    performer_sallary = models.ForeignKey('myuser.UserProfile',
                                          verbose_name='Сотрудник')
    date = models.DateTimeField(verbose_name=u'Дата')
    name_job = models.ForeignKey(JobTitle, to_field='name',
                                 verbose_name='Наименование проекта')
    cost = models.PositiveIntegerField(blank=True, null=True,
                                       verbose_name=u'Ставка в (грн.)')
    number_position = models.PositiveIntegerField(blank=True, null=True,
                                                  verbose_name=u'Кол-во \
                                                  сделанных статей')
    wage = models.PositiveIntegerField(blank=True, null=True,
                                       verbose_name=u'Сумма з/п')
    note = models.CharField(blank=True, null=True,
                            max_length=255, verbose_name=u'Примечание')
    object_delete = models.BooleanField(default=False,
                                        verbose_name=u'Отметка об удалении')

    class Meta:
        verbose_name = u'Зарплата'
        verbose_name_plural = u'Зарплата'
        ordering = ['-date']
        db_table = 'wages'
        permissions = (('perms_sallary', u'права на зарплату'),)

    def __unicode__(self):
        return u'%s' % self.performer_sallary


class ReportUsers(models.Model):
    '''Отчеты'''
    performer = models.ForeignKey('myuser.UserProfile',
                                  verbose_name='Исполнитель')
    date_report = models.DateTimeField(verbose_name=u'Дата создания отчета')
    name_job = models.ForeignKey(JobTitle, to_field='name',
                                 verbose_name='Наименование работы')
    url = models.URLField(max_length=255, blank=True, null=True,
                          verbose_name=u'Ссылка на отчет в GoogleDocs')
    number_symbols = models.PositiveIntegerField(blank=True, null=True,
                                                 verbose_name=u'Количество \
                                                  напечатанных символов')
    note = models.CharField(blank=True, null=True,
                            max_length=255, verbose_name=u'Прим.')
    signature_accept = models.BooleanField(verbose_name="Отметка о проверке \
                                           отчета", default=False)
    object_delete = models.BooleanField(default=False,
                                        verbose_name=u'Отметка об удалении')

    class Meta:
        verbose_name = u'Отчеты'
        verbose_name_plural = u'Отчеты'
        ordering = ['-date_report']
        db_table = 'reportusers'
        permissions = (('perms_url', u'права на отчеты'),)

    def __unicode__(self):
        return u'%s' % self.url
