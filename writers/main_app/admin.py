# coding: utf8
from django.contrib import admin
from .models import JobTitle, Wages, ReportUsers


class JobTitleAdmin(admin.ModelAdmin):
    list_display = ('name', 'date_execution', 'get_performers',
                    'object_delete')

    def get_performers(self, obj):
        return '%s' % "\n".join([p.first_name for p in obj.performers.all()])
    get_performers.short_description = 'Исполнители'


class WagesAdmin(admin.ModelAdmin):
    list_display = ('date', 'performer_sallary', 'object_delete')



class ReportUsersAdmin(admin.ModelAdmin):
    list_display = ('date_report', 'performer', 'get_name_job',)

    def get_name_job(self, obj):
        return '%s' % (obj.name_job.name)
    get_name_job.short_description = 'Наименование работы'

admin.site.register(JobTitle, JobTitleAdmin)
admin.site.register(ReportUsers, ReportUsersAdmin)
admin.site.register(Wages, WagesAdmin)
