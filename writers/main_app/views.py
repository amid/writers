# coding: utf8
from .models import JobTitle, ReportUsers, Wages
from .forms import FormsAddWages
from django.views.generic import ListView, DetailView, CreateView
from writers.myuser.models import UserProfile
from django.http import HttpResponseRedirect
from django.http import Http404


class ListJobs(ListView):
    model = JobTitle
    template_name = 'base.html'

    def get_context_data(self, **kwargs):
        context_data = super(ListJobs, self).get_context_data(**kwargs)
        staff_report = ReportUsers.objects.all()
        context_data['staff_report'] = staff_report
        return context_data


class AddSallary(CreateView):
    model = Wages
    form_class = FormsAddWages
    template_name = 'create_sallary.html'
    success_url = '/sallary/'
    
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


class Sallary(ListView):
    model = Wages
    template_name = 'sallary.html'
    context_object_name = 'sallary_performers'

    def get(self, request, *args, **kwargs):
        qkwargs = {}
        for key in ['type_performer']:
            if kwargs.has_key(key):
                qkwargs[key] = kwargs[key]
        self.queryset = Wages.objects.all()
        print kwargs
        if kwargs.has_key('type_performer'):
            if kwargs['type_performer']==u'freelancer':
                self.type = 'Фрилансеры'
                self.queryset = Wages.objects.filter(performer_sallary__type_performer='freelancer')
        if kwargs.has_key('type_performer'):
            if kwargs['type_performer']==u'office':
                self.type = 'Офисные сотрудники'
                self.queryset = Wages.objects.filter(performer_sallary__type_performer='office')
        try:
            self.performer = '%s' % (
                getattr(self, 'type', 'Все сотрудники'),
            )
        except:
            pass
        return super(Sallary, self).get(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context_data = super(Sallary, self).get_context_data(**kwargs)
        context_data['performer'] = self.performer
        return context_data


class Performers(ListView):
    model = UserProfile
    template_name = 'performers.html'
    context_object_name = 'list_performers'

    def get(self, request, *args, **kwargs):
        qkwargs = {}
        for key in ['type_performer']:
            if kwargs.has_key(key):
                qkwargs[key] = kwargs[key]
        self.queryset = self.model.objects.filter(**qkwargs)
        print kwargs
        if kwargs.has_key('type_performer'):
            if kwargs['type_performer']==u'freelancer':
                self.type = 'Фрилансеры'
        if kwargs.has_key('type_performer'):
            if kwargs['type_performer']==u'office':
                self.type = 'Офисные сотрудники'
        try:
            self.performer = '%s' % (
                getattr(self, 'type', 'Все сотрудники'),
            )
        except:
            pass
        return super(Performers, self).get(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        context_data = super(Performers, self).get_context_data(**kwargs)
        context_data['performer'] = self.performer
        return context_data


class CloseReport(DetailView):
    # закрыть отчет
    model = ReportUsers
    template_name = 'close_report.html'
    
    def get(self, request, *args, **kwargs):
        obj = self.get_object()
        if request.user.is_superuser:
            return super(CloseReport, self).get(request, *args, **kwargs)
        else:
            return HttpResponseRedirect('/')
    
    def post(self, request, *args, **kwargs):
        if self.request.user.is_authenticated():
            obj = self.get_object()
            get_referer = request.META.get('HTTP_REFERER')
            obj.signature_accept = True
            obj.save()
            if get_referer != None:
                return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
            if get_referer == None:
                return HttpResponseRedirect('/')
            # return HttpResponseRedirect(self.get_success_url())
        else:
            raise Http404
