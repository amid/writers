# coding: utf8

from django.conf import settings
from writers.main_app.models import JobTitle
from writers.myuser.models import UserProfile

def utils(request):
    return {
        'title': 'Creative Group',
        'my_settings': settings,
        'jobs': JobTitle.objects.all(),
        'staff': UserProfile.objects.all()
    }
