# coding: utf8
from django.conf.urls.static import static
from django.contrib import admin
from django.conf import settings
admin.autodiscover()
from django.conf.urls import include, url
from main_app.views import ListJobs, Performers, CloseReport, Sallary, AddSallary
from main_app.ajax import xhr_test

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', ListJobs.as_view()),
    url(r'^close_report/(?P<pk>\d{1,9})/$', CloseReport.as_view()),
    url(r'^close_report_save/(?P<pk>\d{1,9})/$', CloseReport.as_view()),
    url(r'^performers/$',  Performers.as_view()),
    url(r'^performers/(?P<type_performer>\w+)/$', Performers.as_view()),
    url(r'^sallary/$',  Sallary.as_view()),
    url(r'^sallary/(?P<type_performer>\w+)/$', Sallary.as_view()),
    url(r'^create_sallary/$', AddSallary.as_view()),
    url(r'^create_sallary_save/$', AddSallary.as_view()),
    url(r'^accounts/login/',
        'django.contrib.auth.views.login'),
    url(r'^accounts/logout/$',
        'django.contrib.auth.views.logout', {'next_page': '/'}),
    url(r'^xhr_test/$', xhr_test),
]


if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL,
                          document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.MEDIA_ROOT,
                          document_root=settings.MEDIA_ROOT)
    urlpatterns += static(settings.STATIC_URL,
                          document_root=settings.STATIC_ROOT)
