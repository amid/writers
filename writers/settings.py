# coding: utf8
import os

BASE_DIR = os.path.dirname(os.path.realpath(__file__))
print BASE_DIR

SECRET_KEY = 'r7)_r8y9#e#c823i+hue1(8q6(n0*x&93wu&m9#l@kw325=f-w'

DEBUG = True

AUTH_USER_MODEL = 'myuser.UserProfile'

AUTH_PROFILE_MODULE = 'myuser.UserProfile'

ALLOWED_HOSTS = []

INSTALLED_APPS = (
    'writers.main_app',
    'writers.myuser',
    'autocomplete_light',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
)

ROOT_URLCONF = 'writers.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR+"/templates", ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.core.context_processors.i18n',
                'django.core.context_processors.media',
                'django.core.context_processors.static',
                'django.contrib.messages.context_processors.messages',
                'writers.processors.context_processors.utils',
            ],
        },
    },
]

WSGI_APPLICATION = 'writers.wsgi.application'

LANGUAGE_CODE = 'ru'

TIME_ZONE = 'Europe/Kiev'

USE_I18N = True

USE_L10N = True

USE_TZ = True

ADMIN_MEDIA_PREFIX = os.path.join(BASE_DIR, 'production', 'static')

MEDIA_ROOT = os.path.join(BASE_DIR, 'production', 'media')

MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(BASE_DIR, 'production', 'static')

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

try:
    execfile(os.path.join(BASE_DIR, 'settings_local.py'),
             globals(), locals())
except IOError:
    pass

